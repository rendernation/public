# Exposing Windows OpenVPN Connect Client's Network to OpenVPN Server's Networks

In addition to including local subnets for specific users on OpenVPN server settings there are also changes that need to be made on client machines running the OpenVPN Connect software. These are not well documented for Windows OS, hence this document.

This document shows you the necessary changes needed to connect remote hosts/guests to your local network using Windows Firewall.

## Prerequisites

- Connection established via OpenVPN connect
- Remote guest/host on the same network as the OpenVPN Active Server

> **N.B** OpenVPN creates a TAP device, which appears in Windows Network Adapters as `Ethernet`, with a `Public` network type.

## Enable IP Forwarding

This will allow the OpenVPN Server's network to see the Clients.

- Click `start`, type `regedit`, and click on its icon
- Navigate through the tree to `Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip`
- In the right panel `double click` on `IPEnableRouter`
- Change the `value data` from `0` to `1`, then `OK`
- Keep regedit open for the next section

> **N.B.** If the registry key does not exist see [this KB article](https://support.microsoft.com/en-us/kb/99686) from Microsoft. In addition, you must be able to restart the machine you are editing.

## Set OpenVPN Device type

This will allow appropriate firewall rules to be set in bulk, and is faster than changing individual entries when the OpenVPN server's network is trusted.

- Click `Start`, type `Network and Sharing Center`, and click on its icon
- Observe the `View Active Networks Panel`
- Look for the connection `Ethernet` connection labeled `Public network` and make a note of it's **name**

![](http://i.imgur.com/ZwPRawa.png)

> If the type is highlighted blue, you may just click it and change the type from `public` to `private` (Windows 8 and 10) / `work` (Windows 7)

- In `regedit` navigate to `Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkList\Profiles\`
- Select each of the branches in the tree view
- Review the contents of each branch in the right panel, until you find one that has the name of the `TAP adapter`
- In the right panel `double click` on `Catergory`
- Change the value data from `0` to `1`

## Test the Network Type

- Disconnect the `OpenVPN Connect` tool from you `myvfxcloud` project
- Reconnect to your `myvfxcloud` project via the `OpenVPN Connect` tool
- Click `Start` and type `Network and Sharing Centre` and click on its icon
- You should observe the `View Active Networks Panel` entry for the `Public network` is now listed as `Private Network` (Windows 8 and 10) / `Work Network` (Windows 7)

![](http://i.imgur.com/Wv0p4qv.png)

## Allow ICMP (ping replies)

To allow pings for troubleshooting issues follow this [Microsoft KB article](https://technet.microsoft.com/en-us/library/cc972926(v=ws.10).aspx) to allow **ICMPv4** from _all_ networks (Scope tab) on all profiles (Advanced tab).

## Authorise the Remote Network for shares

Whilst our changes are complete we also need to modify the SAMBA sharing rules to include the remote network's subnet.

- Click `Start` and Type `Windows Firewall with Advanced Security`
- Click on its icon
- Click on `Inbound Rules`
- Organise by `Name` by clicking the column and locate `File and Printer Sharing (SMB-In)` with the `Profile` `Private`

![](http://i.imgur.com/7zKkXI9.png)

- Double click it and select the `Scope` tab
- Under `Remote IP Address` panel

![](http://i.imgur.com/xxMk1b5.png)

- Click add and enter the remote subnet and bits

![](http://i.imgur.com/9HRnXfP.png)

- Click `OK`
- Click `OK` to save the changes

Machines in the remote subnet should now be able to access the local shares.